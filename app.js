var taskApp = function () {
  'use strict';

  var model = {
    add: function (task) {
      var task = task.trim()
      if (task) {
        tasks.push(task)
      }
    },
    delete: function (index) {
      if (index > -1) {
        tasks.splice(index,1)
      }
    },
    getTasks: function () {
      return tasks || [];
    },
    safe_tags: function(str) {
      return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') ;
    }
  }

  var view = {
    update: function (model) {
      var tasks = model.getTasks(),
          html = '';
      $header.innerHTML = '<span class="triple">Tasks</span> <span class="lift"> (' + tasks.length + ')</span>'
      for (var i = 0; i < tasks.length; i++) {
        html += '<li data-value=' + i + '><button class="deleteTaskItem">X</button> ' + model.safe_tags(tasks[i]) + '</li>'
      };
      $tasks.innerHTML = html
    }
  }

  var controller = {
    initialize: function () {
      controller.formConstructor();
      $tasksDiv.addEventListener('click', function (e) {
        if (event.target.className === 'deleteTaskItem') {
          var task = event.target.parentNode
          var index = task.getAttribute('data-value')
          controller.delete(index)
        };
      })
      $form.addEventListener('submit', function (e) {
        e.preventDefault()
        controller.add()
      });
    },
    formConstructor: function () {
      $parentNode.appendChild($header);
      $parentNode.appendChild($form);
      $form.appendChild($input);
      $form.appendChild($addButton);
      $parentNode.appendChild($tasksDiv);
      $tasksDiv.appendChild($tasks);
    },
    findParentNode: function () {
      var scriptTag = document.getElementsByTagName('script');
      scriptTag = scriptTag[scriptTag.length - 1];
      return scriptTag.parentNode
    },
    add: function (e) {
      model.add($input.value);
      $input.value = '';
      view.update(model);
    },
    delete: function (index) {
      model.delete(index);
      view.update(model);
    }
  };

  var tasks = [],
      $parentNode = controller.findParentNode(),
      $header = document.createElement("H1"),
      $tasksDiv = document.createElement("DIV"),
      $tasks = document.createElement("UL"),
      $form = document.createElement('FORM'),
      $input = document.createElement("input"),
      $addButton = document.createElement("button"),
      $deleteButton = document.createElement("button");

  $header.innerHTML = '<span class="triple">Tasks</span> <span class="lift"> (' + tasks.length + ')</span>'
  $tasksDiv.className += "tasks"
  $tasks.id = 'tasks'
  $addButton.innerText = "Add"

  window.addEventListener('load', controller.initialize())
};
